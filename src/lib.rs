pub mod points;
pub mod shapes;
pub mod values;

use shapes::{ObjectValues, ShapeGroup};

#[cfg(feature = "data-stream")]
use data_stream::{FromStream, ToStream, collections::SizeSettings, from_stream, to_stream};
use simple_color::Color;
use vector_space::InnerSpace;

use std::fmt::{Debug, Formatter, Result as FmtResult};
#[cfg(feature = "data-stream")]
use std::io::{Error, ErrorKind, Read, Write};

#[derive(Clone)]
pub struct World<V: InnerSpace> {
    pub shape_groups: Vec<ShapeGroup<V>>,
    pub background_color: Color,
}

impl<V: InnerSpace> World<V> {
    pub fn empty() -> Self {
        Self {
            shape_groups: Vec::new(),
            background_color: Color::BLACK,
        }
    }

    pub fn new(shape_groups: Vec<ShapeGroup<V>>, background_color: Color) -> Self {
        Self {
            shape_groups,
            background_color,
        }
    }

    pub fn add_object(
        &mut self,
        group: usize,
        index: usize,
        parameters: Vec<V::Scalar>,
        points: Vec<V>,
        colors: Vec<Color>,
    ) {
        let group = &mut self.shape_groups[group];
        let objects = &mut group.objects;

        while objects.len() <= index {
            objects.push(None);
        }

        objects[index] = Some(ObjectValues {
            parameters,
            points,
            colors,
        });
    }

    pub fn remove_object(&mut self, group: usize, index: usize) {
        self.shape_groups[group].objects[index] = None;
    }
}

impl<V: Debug + InnerSpace> Debug for World<V>
where
    V::Scalar: Debug,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        f.debug_struct("World")
            .field("shape_groups", &self.shape_groups)
            .field("background_color", &self.background_color)
            .finish()
    }
}

#[cfg(feature = "data-stream")]
impl<S: SizeSettings, V: ToStream<S> + InnerSpace> ToStream<S> for World<V>
where
    V::Scalar: ToStream<S>,
{
    fn to_stream<W: Write>(&self, writer: &mut W) -> Result<(), Error> {
        let Self {
            shape_groups,
            background_color,
        } = self;

        to_stream::<S, _, _>(shape_groups, writer)?;
        to_stream::<S, _, _>(background_color, writer)?;

        Ok(())
    }
}

#[cfg(feature = "data-stream")]
impl<S: SizeSettings, V: FromStream<S> + InnerSpace> FromStream<S> for World<V>
where
    V::Scalar: FromStream<S>,
{
    fn from_stream<R: Read>(reader: &mut R) -> Result<Self, Error> {
        Ok(Self {
            shape_groups: from_stream::<S, _, _>(reader)?,
            background_color: from_stream::<S, _, _>(reader)?,
        })
    }
}

#[derive(Clone)]
pub enum WorldMessage<V: InnerSpace> {
    Load(World<V>),
    SetPaused(bool),
    SetSlowdown(f32),
    RefreshPoints {
        group: usize,
        index: usize,
        points: Vec<V>,
    },
    RefreshParameters {
        group: usize,
        index: usize,
        parameters: Vec<V::Scalar>,
    },
    AddObject {
        group: usize,
        index: usize,
        parameters: Vec<V::Scalar>,
        points: Vec<V>,
        colors: Vec<Color>,
    },
    RemoveObject {
        group: usize,
        index: usize,
    },
}

impl<V: Debug + InnerSpace> Debug for WorldMessage<V>
where
    V::Scalar: Debug,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        use WorldMessage::*;
        match self {
            Load(world) => f.debug_tuple("World").field(world).finish(),

            SetPaused(paused) => f.debug_tuple("SetPaused").field(paused).finish(),
            SetSlowdown(slowdown) => f.debug_tuple("SetSlowdown").field(slowdown).finish(),
            RefreshParameters {
                group,
                index,
                parameters,
            } => f
                .debug_struct("RefreshParameters")
                .field("group", group)
                .field("index", index)
                .field("parameters", parameters)
                .finish(),
            RefreshPoints {
                group,
                index,
                points,
            } => f
                .debug_struct("RefreshPoints")
                .field("group", group)
                .field("index", index)
                .field("points", points)
                .finish(),
            AddObject {
                group,
                index,
                parameters,
                points,
                colors,
            } => f
                .debug_struct("AddObject")
                .field("group", group)
                .field("index", index)
                .field("parameters", parameters)
                .field("points", points)
                .field("colors", colors)
                .finish(),
            RemoveObject { group, index } => f
                .debug_struct("RemoveObject")
                .field("group", group)
                .field("index", index)
                .finish(),
        }
    }
}

#[cfg(feature = "data-stream")]
impl<S: SizeSettings, V: ToStream<S> + InnerSpace> ToStream<S> for WorldMessage<V>
where
    V::Scalar: ToStream<S>,
{
    fn to_stream<W: Write>(&self, writer: &mut W) -> Result<(), Error> {
        use WorldMessage::*;
        match self {
            Load(world) => {
                to_stream::<S, _, _>(&0u8, writer)?;
                to_stream::<S, _, _>(world, writer)?;
            }
            SetPaused(paused) => {
                to_stream::<S, _, _>(&1u8, writer)?;
                to_stream::<S, _, _>(paused, writer)?;
            }
            SetSlowdown(slowdown) => {
                to_stream::<S, _, _>(&2u8, writer)?;
                to_stream::<S, _, _>(slowdown, writer)?;
            }
            RefreshParameters {
                group,
                index,
                parameters,
            } => {
                to_stream::<S, _, _>(&3u8, writer)?;
                to_stream::<S, _, _>(group, writer)?;
                to_stream::<S, _, _>(index, writer)?;
                to_stream::<S, _, _>(parameters, writer)?;
            }
            RefreshPoints {
                group,
                index,
                points,
            } => {
                to_stream::<S, _, _>(&4u8, writer)?;
                to_stream::<S, _, _>(group, writer)?;
                to_stream::<S, _, _>(index, writer)?;
                to_stream::<S, _, _>(points, writer)?;
            }
            AddObject {
                group,
                index,
                parameters,
                points,
                colors,
            } => {
                to_stream::<S, _, _>(&5u8, writer)?;
                to_stream::<S, _, _>(group, writer)?;
                to_stream::<S, _, _>(index, writer)?;
                to_stream::<S, _, _>(parameters, writer)?;
                to_stream::<S, _, _>(points, writer)?;
                to_stream::<S, _, _>(colors, writer)?;
            }
            RemoveObject { group, index } => {
                to_stream::<S, _, _>(&6u8, writer)?;
                to_stream::<S, _, _>(group, writer)?;
                to_stream::<S, _, _>(index, writer)?;
            }
        }

        Ok(())
    }
}

#[cfg(feature = "data-stream")]
impl<S: SizeSettings, V: FromStream<S> + InnerSpace> FromStream<S> for WorldMessage<V>
where
    V::Scalar: FromStream<S>,
{
    fn from_stream<R: Read>(reader: &mut R) -> Result<Self, Error> {
        use WorldMessage::*;
        match from_stream::<S, u8, _>(reader)? {
            0 => Ok(Load(from_stream::<S, _, _>(reader)?)),
            1 => Ok(SetPaused(from_stream::<S, _, _>(reader)?)),
            2 => Ok(SetSlowdown(from_stream::<S, _, _>(reader)?)),
            3 => Ok(RefreshParameters {
                group: from_stream::<S, _, _>(reader)?,
                index: from_stream::<S, _, _>(reader)?,
                parameters: from_stream::<S, _, _>(reader)?,
            }),
            4 => Ok(RefreshPoints {
                group: from_stream::<S, _, _>(reader)?,
                index: from_stream::<S, _, _>(reader)?,
                points: from_stream::<S, _, _>(reader)?,
            }),
            5 => Ok(AddObject {
                group: from_stream::<S, _, _>(reader)?,
                index: from_stream::<S, _, _>(reader)?,
                parameters: from_stream::<S, _, _>(reader)?,
                points: from_stream::<S, _, _>(reader)?,
                colors: from_stream::<S, _, _>(reader)?,
            }),
            6 => Ok(RemoveObject {
                group: from_stream::<S, _, _>(reader)?,
                index: from_stream::<S, _, _>(reader)?,
            }),
            _ => Err(Error::new(ErrorKind::InvalidInput, "Invalid element type")),
        }
    }
}
