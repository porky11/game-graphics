use super::values::{Expression, Value};

#[cfg(feature = "data-stream")]
use data_stream::{FromStream, ToStream, collections::SizeSettings, from_stream, to_stream};
use num_traits::real::Real;
use vector_basis::{Bases as _, Basis};
use vector_space::{InnerSpace, VectorSpace};

#[cfg(feature = "data-stream")]
use std::io::{Error, Read, Write};

#[derive(Clone, Debug)]
pub struct Point<N> {
    start: usize,
    end: usize,
    ratio: Value<N>,
    parallel: Expression<N>,
    orthogonal: Expression<N>,
}

impl<N: Real> Point<N> {
    pub fn new(point: usize) -> Self {
        Self {
            start: point,
            end: point,
            ratio: Value::Static(N::zero()),
            parallel: Value::Static(N::zero()).into(),
            orthogonal: Value::Static(N::zero()).into(),
        }
    }

    pub fn segment(start: usize, end: usize, ratio: Value<N>) -> Self {
        Self {
            start,
            end,
            ratio,
            parallel: Value::Static(N::zero()).into(),
            orthogonal: Value::Static(N::zero()).into(),
        }
    }

    pub fn with_parallel(self, parallel: Expression<N>) -> Self {
        Self { parallel, ..self }
    }

    pub fn with_orthogonal(self, orthogonal: Expression<N>) -> Self {
        Self { orthogonal, ..self }
    }

    pub fn position<V: VectorSpace<Scalar = N> + Basis<0> + Basis<1> + InnerSpace>(
        &self,
        points: &[V],
        parameters: &[N],
    ) -> V {
        let start = points[self.start];
        let end = points[self.end];
        let ratio = self.ratio.get(parameters);
        let line = end - start;

        if line.is_zero() {
            return start;
        }

        let dir = line.normalize();
        let mut away = dir;
        *away.bases_mut::<0>() = dir.bases::<1>();
        *away.bases_mut::<1>() = -dir.bases::<0>();
        start
            + line * ratio
            + dir * self.parallel.eval(parameters)
            + away * self.orthogonal.eval(parameters)
    }
}

impl<N: Real> From<usize> for Point<N> {
    fn from(pos: usize) -> Self {
        Self::new(pos)
    }
}

#[cfg(feature = "data-stream")]
impl<S: SizeSettings, N: ToStream<S>> ToStream<S> for Point<N> {
    fn to_stream<W: Write>(&self, writer: &mut W) -> Result<(), Error> {
        to_stream::<S, _, _>(&self.start, writer)?;
        to_stream::<S, _, _>(&self.end, writer)?;
        to_stream::<S, _, _>(&self.ratio, writer)?;
        to_stream::<S, _, _>(&self.parallel, writer)?;
        to_stream::<S, _, _>(&self.orthogonal, writer)?;

        Ok(())
    }
}

#[cfg(feature = "data-stream")]
impl<S: SizeSettings, N: FromStream<S>> FromStream<S> for Point<N> {
    fn from_stream<R: Read>(reader: &mut R) -> Result<Self, Error> {
        Ok(Self {
            start: from_stream::<S, _, _>(reader)?,
            end: from_stream::<S, _, _>(reader)?,
            ratio: from_stream::<S, _, _>(reader)?,
            parallel: from_stream::<S, _, _>(reader)?,
            orthogonal: from_stream::<S, _, _>(reader)?,
        })
    }
}
