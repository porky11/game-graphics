use super::{
    points::Point,
    values::{Expression, Value},
};

#[cfg(feature = "data-stream")]
use data_stream::{FromStream, ToStream, collections::SizeSettings, from_stream, to_stream};
use num_traits::real::Real;
use simple_color::Color;
use vector_basis::Basis;
use vector_space::{InnerSpace, VectorSpace};

#[cfg(feature = "data-stream")]
use std::io::{Error, ErrorKind, Read, Write};

#[derive(Clone, Default, Debug)]
pub struct LineData<N, C> {
    pub width: N,
    pub color: C,
}

#[cfg(feature = "data-stream")]
impl<S: SizeSettings, N: ToStream<S>, C: ToStream<S>> ToStream<S> for LineData<N, C> {
    fn to_stream<W: Write>(&self, writer: &mut W) -> Result<(), Error> {
        to_stream::<S, _, _>(&self.width, writer)?;
        to_stream::<S, _, _>(&self.color, writer)?;

        Ok(())
    }
}

#[cfg(feature = "data-stream")]
impl<S: SizeSettings, N: FromStream<S>, C: FromStream<S>> FromStream<S> for LineData<N, C> {
    fn from_stream<R: Read>(reader: &mut R) -> Result<Self, Error> {
        Ok(Self {
            width: from_stream::<S, _, _>(reader)?,
            color: from_stream::<S, _, _>(reader)?,
        })
    }
}

#[derive(Clone, Debug)]
pub enum ShapeKind<N, C> {
    Line,
    Curve,
    Filled(C),
    Blob(C),
    Bloated { rad: N, color: C },
}

#[cfg(feature = "data-stream")]
impl<S: SizeSettings, N: ToStream<S>, C: ToStream<S>> ToStream<S> for ShapeKind<N, C> {
    fn to_stream<W: Write>(&self, writer: &mut W) -> Result<(), Error> {
        use ShapeKind::*;
        match self {
            Line => to_stream::<S, _, _>(&0u8, writer)?,
            Curve => to_stream::<S, _, _>(&1u8, writer)?,
            Filled(color) => {
                to_stream::<S, _, _>(&2u8, writer)?;
                to_stream::<S, _, _>(color, writer)?;
            }
            Blob(color) => {
                to_stream::<S, _, _>(&3u8, writer)?;
                to_stream::<S, _, _>(color, writer)?;
            }
            Bloated { rad, color } => {
                to_stream::<S, _, _>(&4u8, writer)?;
                to_stream::<S, _, _>(rad, writer)?;
                to_stream::<S, _, _>(color, writer)?;
            }
        }

        Ok(())
    }
}

#[cfg(feature = "data-stream")]
impl<S: SizeSettings, N: FromStream<S>, C: FromStream<S>> FromStream<S> for ShapeKind<N, C> {
    fn from_stream<R: Read>(reader: &mut R) -> Result<Self, Error> {
        use ShapeKind::*;
        Ok(match from_stream::<S, u8, _>(reader)? {
            0 => Line,
            1 => Curve,
            2 => Filled(from_stream::<S, _, _>(reader)?),
            3 => Blob(from_stream::<S, _, _>(reader)?),
            4 => Bloated {
                rad: from_stream::<S, _, _>(reader)?,
                color: from_stream::<S, _, _>(reader)?,
            },
            _ => return Err(Error::new(ErrorKind::InvalidInput, "Invalid element type")),
        })
    }
}

#[derive(Clone, Debug)]
pub struct ShapeData<N, V, C> {
    pub kind: ShapeKind<N, C>,
    pub points: Vec<V>,
    pub line: LineData<N, C>,
}

#[cfg(feature = "data-stream")]
impl<S: SizeSettings, N: ToStream<S>, V: ToStream<S>, C: ToStream<S>> ToStream<S>
    for ShapeData<N, V, C>
{
    fn to_stream<W: Write>(&self, writer: &mut W) -> Result<(), Error> {
        to_stream::<S, _, _>(&self.kind, writer)?;
        to_stream::<S, _, _>(&self.points, writer)?;
        to_stream::<S, _, _>(&self.line, writer)?;

        Ok(())
    }
}

#[cfg(feature = "data-stream")]
impl<S: SizeSettings, N: FromStream<S>, V: FromStream<S>, C: FromStream<S>> FromStream<S>
    for ShapeData<N, V, C>
{
    fn from_stream<R: Read>(reader: &mut R) -> Result<Self, Error> {
        Ok(Self {
            kind: from_stream::<S, _, _>(reader)?,
            points: from_stream::<S, _, _>(reader)?,
            line: from_stream::<S, _, _>(reader)?,
        })
    }
}

pub type Shape<N> = ShapeData<Expression<N>, Point<N>, Value<Color>>;

impl<N: Real> Shape<N> {
    pub fn eval<V: VectorSpace<Scalar = N> + InnerSpace + Basis<0> + Basis<1>>(
        &self,
        values: &ObjectValues<V::Scalar, V>,
    ) -> ShapeData<V::Scalar, V, Color> {
        let points = if self.points.is_empty() {
            values.points.clone()
        } else {
            self.points
                .iter()
                .map(|point| point.position(&values.points, &values.parameters))
                .collect()
        };

        let line = LineData {
            width: self.line.width.eval(&values.parameters),
            color: self.line.color.get(&values.colors),
        };

        use ShapeKind::*;
        let kind = match &self.kind {
            Line => Line,
            Curve => Curve,
            Filled(color) => Filled(color.get(&values.colors)),
            Blob(color) => Blob(color.get(&values.colors)),
            Bloated { rad, color } => Bloated {
                rad: rad.eval(&values.parameters),
                color: color.get(&values.colors),
            },
        };

        ShapeData { kind, points, line }
    }
}

#[derive(Clone, Debug)]
pub struct ObjectValues<N, V> {
    pub parameters: Vec<N>,
    pub points: Vec<V>,
    pub colors: Vec<Color>,
}

#[cfg(feature = "data-stream")]
impl<S: SizeSettings, V: ToStream<S> + InnerSpace> ToStream<S> for ObjectValues<V::Scalar, V>
where
    V::Scalar: ToStream<S>,
{
    fn to_stream<W: Write>(&self, writer: &mut W) -> Result<(), Error> {
        to_stream::<S, _, _>(&self.parameters, writer)?;
        to_stream::<S, _, _>(&self.points, writer)?;
        to_stream::<S, _, _>(&self.colors, writer)?;

        Ok(())
    }
}

#[cfg(feature = "data-stream")]
impl<S: SizeSettings, V: FromStream<S> + InnerSpace> FromStream<S> for ObjectValues<V::Scalar, V>
where
    V::Scalar: FromStream<S>,
{
    fn from_stream<R: Read>(reader: &mut R) -> Result<Self, Error> {
        Ok(Self {
            parameters: from_stream::<S, _, _>(reader)?,
            points: from_stream::<S, _, _>(reader)?,
            colors: from_stream::<S, _, _>(reader)?,
        })
    }
}

#[derive(Clone, Debug)]
pub struct ShapeGroup<V: InnerSpace> {
    pub shapes: Vec<Shape<V::Scalar>>,
    pub objects: Vec<Option<ObjectValues<V::Scalar, V>>>,
}

#[cfg(feature = "data-stream")]
impl<S: SizeSettings, V: ToStream<S> + InnerSpace> ToStream<S> for ShapeGroup<V>
where
    V::Scalar: ToStream<S>,
{
    fn to_stream<W: Write>(&self, writer: &mut W) -> Result<(), Error> {
        to_stream::<S, _, _>(&self.shapes, writer)?;
        to_stream::<S, _, _>(&self.objects, writer)?;

        Ok(())
    }
}

#[cfg(feature = "data-stream")]
impl<S: SizeSettings, V: FromStream<S> + InnerSpace> FromStream<S> for ShapeGroup<V>
where
    V::Scalar: FromStream<S>,
{
    fn from_stream<R: Read>(reader: &mut R) -> Result<Self, Error> {
        Ok(Self {
            shapes: from_stream::<S, _, _>(reader)?,
            objects: from_stream::<S, _, _>(reader)?,
        })
    }
}
