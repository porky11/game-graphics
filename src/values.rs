#[cfg(feature = "data-stream")]
use data_stream::{FromStream, ToStream, collections::SizeSettings, from_stream, to_stream};
use num_traits::real::Real;

#[cfg(feature = "data-stream")]
use std::io::{Error, ErrorKind, Read, Write};

#[derive(Copy, Clone, Debug)]
pub enum Value<V> {
    Static(V),
    Dynamic(usize),
}

impl<V> From<usize> for Value<V> {
    fn from(pos: usize) -> Self {
        Self::Dynamic(pos)
    }
}

impl<V: Copy> Value<V> {
    pub fn get(&self, points: &[V]) -> V {
        use Value::*;
        match *self {
            Static(value) => value,
            Dynamic(index) => points[index],
        }
    }
}

#[cfg(feature = "data-stream")]
impl<S: SizeSettings, V: ToStream<S>> ToStream<S> for Value<V> {
    fn to_stream<W: Write>(&self, writer: &mut W) -> Result<(), Error> {
        use Value::*;
        match self {
            Static(value) => {
                to_stream::<S, _, _>(&0u8, writer)?;
                to_stream::<S, _, _>(value, writer)?;
            }
            Dynamic(index) => {
                to_stream::<S, _, _>(&1u8, writer)?;
                to_stream::<S, _, _>(index, writer)?;
            }
        }

        Ok(())
    }
}

#[cfg(feature = "data-stream")]
impl<S: SizeSettings, V: FromStream<S>> FromStream<S> for Value<V> {
    fn from_stream<R: Read>(reader: &mut R) -> Result<Self, Error> {
        use Value::*;
        Ok(match from_stream::<S, u8, _>(reader)? {
            0 => Static(from_stream::<S, _, _>(reader)?),
            1 => Dynamic(from_stream::<S, _, _>(reader)?),
            _ => return Err(Error::new(ErrorKind::InvalidInput, "Invalid element type")),
        })
    }
}

#[derive(Clone, Debug)]
pub enum Expression<N> {
    Value(Value<N>),
    Neg(Box<Expression<N>>),
    Invert(Box<Expression<N>>),
    Mul(Box<Expression<N>>, Box<Expression<N>>),
}

impl<N> From<usize> for Expression<N> {
    fn from(pos: usize) -> Self {
        Self::Value(Value::Dynamic(pos))
    }
}

impl<N> From<Value<N>> for Expression<N> {
    fn from(value: Value<N>) -> Self {
        Self::Value(value)
    }
}

impl<N: Real> Expression<N> {
    pub fn eval(&self, values: &[N]) -> N {
        use Expression::*;
        match self {
            Value(value) => value.get(values),
            Neg(exp) => -exp.eval(values),
            Invert(exp) => N::one() - exp.eval(values),
            Mul(exp1, exp2) => exp1.eval(values) * exp2.eval(values),
        }
    }

    pub fn negate(self) -> Self {
        Self::Neg(Box::new(self))
    }

    pub fn invert(self) -> Self {
        Self::Invert(Box::new(self))
    }

    pub fn multiply(self, other: Self) -> Self {
        Self::Mul(Box::new(self), Box::new(other))
    }
}

#[cfg(feature = "data-stream")]
impl<S: SizeSettings, V: ToStream<S>> ToStream<S> for Expression<V> {
    fn to_stream<W: Write>(&self, writer: &mut W) -> Result<(), Error> {
        use Expression::*;
        match self {
            Value(value) => {
                to_stream::<S, _, _>(&0u8, writer)?;
                to_stream::<S, _, _>(value, writer)?;
            }
            Neg(exp) => {
                to_stream::<S, _, _>(&1u8, writer)?;
                to_stream::<S, _, _>(exp, writer)?;
            }
            Invert(exp) => {
                to_stream::<S, _, _>(&2u8, writer)?;
                to_stream::<S, _, _>(exp, writer)?;
            }
            Mul(exp1, exp2) => {
                to_stream::<S, _, _>(&3u8, writer)?;
                to_stream::<S, _, _>(exp1, writer)?;
                to_stream::<S, _, _>(exp2, writer)?;
            }
        }

        Ok(())
    }
}

#[cfg(feature = "data-stream")]
impl<S: SizeSettings, V: FromStream<S>> FromStream<S> for Expression<V> {
    fn from_stream<R: Read>(reader: &mut R) -> Result<Self, Error> {
        use Expression::*;
        Ok(match from_stream::<S, u8, _>(reader)? {
            0 => Value(from_stream::<S, _, _>(reader)?),
            1 => Neg(from_stream::<S, _, _>(reader)?),
            2 => Invert(from_stream::<S, _, _>(reader)?),
            3 => Mul(
                from_stream::<S, _, _>(reader)?,
                from_stream::<S, _, _>(reader)?,
            ),
            _ => return Err(Error::new(ErrorKind::InvalidInput, "Invalid element type")),
        })
    }
}
